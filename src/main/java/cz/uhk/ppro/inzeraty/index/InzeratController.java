package cz.uhk.ppro.inzeraty.index;

import cz.uhk.ppro.inzeraty.model.Inzerat;
import cz.uhk.ppro.inzeraty.sluzby.PametoveUlozisteInzeratu;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;


@Controller
public class InzeratController {
    private PametoveUlozisteInzeratu pui = new PametoveUlozisteInzeratu();

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        model.addAttribute("inzeraty", pui);
        return "index";
    }

    @RequestMapping("/vytvoritInzerat")
    public String vytvoritInzerat(Model model,
                                  @RequestParam("text") String t,
                                  @RequestParam("cats") String c,
                                  @RequestParam("cena") BigDecimal d)
    {
        Inzerat inzerat = new Inzerat();
        inzerat.setKategorie(c);
        inzerat.setCena(d);
        inzerat.setText(t);
        pui.pridej(inzerat);

        model.addAttribute("kod", inzerat.getHesloProUpravu());
        return "kod";
    }

    @RequestMapping("/item")
    public String item(Model model, @RequestParam("id") int id, @RequestParam("idd") String idd)
    {
        if(pui.getById(id).getHesloProUpravu().equals(idd)) {
            model.addAttribute("text", pui.getById(id).getText());
            model.addAttribute("cena", pui.getById(id).getCena());
            model.addAttribute("kategorie", pui.getById(id).getKategorie());
            model.addAttribute("id", id);
            return "item";
        }else {
            return "redirect:http://localhost:8080/index";
        }
    }

    @RequestMapping("/upravitInzerat")
    public String upravitInzerat(Model model,
                                 @RequestParam("text") String t,
                                 @RequestParam("cats") String c,
                                 @RequestParam("cena") BigDecimal d,
                                 @RequestParam("id") int id)
    {
        Inzerat inzerat = new Inzerat();
        inzerat.setKategorie(c);
        inzerat.setCena(d);
        inzerat.setText(t);
        inzerat.setId(id);
        pui.uprav(inzerat);

        model.addAttribute("kod", inzerat.getHesloProUpravu());
        return "kod";
    }

    @RequestMapping("/deleteInzerat")
    public String deleteInzerat(Model model, @RequestParam("id") int id)
    {
        pui.odstran(id);
        return "redirect:http://localhost:8080/index";
    }

    @RequestMapping("/category")
    public String category(Model model, @RequestParam("catss") String catss)
    {
        model.addAttribute("kategorie", pui.getInzeratyByKategorie(catss));
        return "catss";
    }
}
